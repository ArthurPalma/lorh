import { Component, OnInit } from "@angular/core";
import { configService } from "../config.service";

@Component({
  selector: "app-characteristics",
  templateUrl: "./characteristics.component.html",
  styleUrls: ["./characteristics.component.css"],
})
export class CharacteristicsComponent implements OnInit {
  constructor(private configService: configService) {}

  ngOnInit(): void {
    this.characteristicsFromLogFile();
    this.characteristicsFromDataFile();
  }

  entireFileLineByLine: string[];
  crossoverMethod: string;
  mutationMethod: string;
  selectionScheme: string;
  parentSelectionMethod: string;
  survivorSelectionMethod: string;

  randomSeed: string;
  importantSessionRate: string;
  urgentSessionRate: string;
  nbSessionAppication: string;
  Horizon: string;

  characteristicsFromLogFile() {
    /*
  Parse the log file and and initialize the values for displaying the characteristics of the algorithm

  Parameters
  ----------
  none

  Returns
  -------
  none
  */

    this.configService
      .getTextFile("assets/LOG_HIMMELBLAU_FUNCTION.txt")
      .subscribe((results) => {
        this.entireFileLineByLine = results.split("\n");
        let caracteristicArray = [];
        let separateValueArray = [];
        this.entireFileLineByLine.forEach(function (value) {
          if (value.startsWith("P ")) {
            separateValueArray = value.split(".");
            caracteristicArray.push(separateValueArray[2]);
          }
        });
        this.crossoverMethod = caracteristicArray[1];
        this.mutationMethod = caracteristicArray[2];
        this.selectionScheme = caracteristicArray[3];
        this.parentSelectionMethod = caracteristicArray[4];
        this.survivorSelectionMethod = caracteristicArray[5];
      });
  }

  characteristicsFromDataFile() {
    /*
  Parse the data file and initialize the values for displaying the characteristics of the algorithm

  Parameters
  ----------
  none

  Returns
  -------
  none
  */

    this.configService
      .getTextFile("assets/LORH_BLOC_OP_1_0_0_0_1165820616.dat")
      .subscribe((results) => {
        let caracteristicArray = [];
        let separateValueArray = [];
        let dataArray = [];
        caracteristicArray = results.split("\n");
        caracteristicArray.forEach(function (value) {
          if (value.startsWith("#RANDOM")) {
            separateValueArray = value.split("#");
            dataArray[0] = separateValueArray[1];
          }
          if (value.startsWith("#IMPORTANT")) {
            separateValueArray = value.split("#");
            dataArray[1] = separateValueArray[1];
          }
          if (value.startsWith("#URGENT")) {
            separateValueArray = value.split("#");
            dataArray[2] = separateValueArray[1];
          }
          if (value.startsWith("#NB_SESSION")) {
            separateValueArray = value.split("#");
            dataArray[3] = separateValueArray[1];
          }
          if (value.startsWith("#HORIZON")) {
            separateValueArray = value.split("#");
            dataArray[4] = separateValueArray[1];
          }
        });
        this.randomSeed = dataArray[0];
        this.importantSessionRate = dataArray[1];
        this.urgentSessionRate = dataArray[2];
        this.nbSessionAppication = dataArray[3];
        this.Horizon = dataArray[4];
      });
  }
}
