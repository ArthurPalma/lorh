import { Component, ViewChildren, OnInit, QueryList } from "@angular/core";
import { configService } from "../config.service";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import { BaseChartDirective } from "ng2-charts";
import { Label } from "ng2-charts";
declare var $: any;

@Component({
  selector: "app-charts",
  templateUrl: "./charts.component.html",
  styleUrls: ["./charts.component.css"],
})
export class ChartsComponent implements OnInit {
  constructor(private configService: configService) {}
  @ViewChildren(BaseChartDirective)
  charts: QueryList<BaseChartDirective>;

  ngOnInit(): void {
    this.download();
  }

  entireFileLineByLine: string[];
  fitnessMin: number;
  fitnessMax: number;
  entropyMin: number;
  entropyMax: number;
  maxLocal: number;
  minLocal: number;
  globalFitnessMax: number;
  globalFitnessMin: number;
  gapMax: number;
  gapMin: number;
  indexMax: number = 0;
  indexMin: number = 0;
  globalMax: number;
  globalMin: number;
  generationArray = [];
  dataCyan = [];
  dataGreen = [];
  dataBlue = [];
  dataOrange = [];

  download() {
    /*
  Parse the file containing the data and use it to initialize the gauges

  Parameters
  ----------
  none

  Returns
  -------
  none
  */

    this.configService
      .getTextFile("assets/LOG_HIMMELBLAU_FUNCTION.txt")
      .subscribe((results) => {
        this.entireFileLineByLine = results.split("\n");
        let generationValuesArray = [];
        let separateValueArray = [];
        let fitnessArray = [];
        let entropyArray = [];
        let splittedValueArray = [];
        let populationValuesArray = [];
        let listOfLists = [];
        let chunk: number;
        let populationSize = [];
        this.entireFileLineByLine.forEach(function (value) {
          if (value.startsWith("### ")) {
            generationValuesArray.push(value);
          }
        });
        this.entireFileLineByLine.forEach(function (value) {
          if (value.startsWith("P Number")) {
            populationSize = value.split(":");
            chunk = parseFloat(populationSize[1]);
          }
        });
        generationValuesArray.forEach(function (value) {
          separateValueArray = value.split("|");
          fitnessArray.push(separateValueArray[1]);
          entropyArray.push(separateValueArray[2]);
        });
        this.fitnessMax = this.getMax(fitnessArray);
        this.fitnessMin = this.getMin(fitnessArray);
        this.entropyMax = this.getMax(entropyArray);
        this.entropyMin = this.getMin(entropyArray);
        generationValuesArray = [];
        separateValueArray = [];
        this.entireFileLineByLine.forEach(function (value) {
          if (value.startsWith("#IND")) {
            generationValuesArray.push(value);
          }
        });
        this.generationArray = generationValuesArray;
        generationValuesArray.forEach(function (value) {
          splittedValueArray = value.split("|");
          populationValuesArray.push(splittedValueArray[1]);
        });
        for (let index = 0; index < populationValuesArray.length; index += chunk) {
          separateValueArray = populationValuesArray.slice(index, index + chunk);
          listOfLists.push(separateValueArray);
        }
        this.globalFitnessMax = parseFloat(
          this.getMax(listOfLists[0]).toFixed(2)
        );
        this.globalFitnessMin = parseFloat(
          this.getMin(listOfLists[0]).toFixed(2)
        );
        this.globalMax = 0;
        this.globalMin = 0;
        this.gapMax = 0;
        this.gapMin = 1000;
        this.interval(
          fitnessArray,
          entropyArray,
          listOfLists,
          populationValuesArray
        );
      });
  }

  interval(
    fitnessArray: any[],
    entropyArray: any[],
    listOfLists: any[],
    allValueArray: any[]
  ) {
    /*
  initialize the value of the gauges and indicators in the zone 4

  Parameters
  ----------
  fitnessArray : any[]
        the array containing the data related to the fitness
  entropyArray : any[]
      the array containing the data related to the entropy
  listOfLists : any[]
        the array containing the data related to the fitness separed by generations
  allValueArray : any[]
      the array containing the data related to the entropy
  Returns
  -------
  nothing
  */
    let index = 0;
    let parsed = [];
    let sd;
    this.setColumn(listOfLists, allValueArray);
    this.fitnessSortedScatterChart(listOfLists);
    let interval = setInterval(() => {
      listOfLists[index].forEach(function (value) {
        parsed.push(parseFloat(value));
      });
      sd = this.standardDeviation(parsed);
      if (sd > this.gapMax) {
        this.gapMax = parseFloat(sd.toFixed(2));
        this.indexMax = index;
      }
      if (sd < this.gapMin) {
        this.gapMin = parseFloat(sd.toFixed(2));
        this.indexMin = index;
      }
      this.needleValueFitness = Math.round(
        ((parseFloat(fitnessArray[index]) - this.fitnessMin) * 100) /
          (this.fitnessMax - this.fitnessMin)
      );
      this.bottomLabelFitness = parseFloat(
        fitnessArray[index].replace(",", ".")
      ).toFixed(2);
      this.needleValueEntropy = parseFloat(
        (
          ((parseFloat(entropyArray[index].replace(",", ".")) -
            this.entropyMin) *
            100) /
          (this.entropyMax - this.entropyMin)
        ).toFixed(4)
      );
      this.bottomLabelEntropy = this.needleValueEntropy.toString() + "%";
      this.maxLocal = parseFloat(this.getMax(listOfLists[index]).toFixed(2));
      this.minLocal = parseFloat(this.getMin(listOfLists[index]).toFixed(2));
      if (this.getMax(listOfLists[index]) > this.globalFitnessMax) {
        this.globalFitnessMax = parseFloat(
          this.getMax(listOfLists[index]).toFixed(2)
        );
        this.globalMax = index;
      }
      if (this.getMin(listOfLists[index]) < this.globalFitnessMin) {
        this.globalFitnessMin = parseFloat(
          this.getMin(listOfLists[index]).toFixed(2)
        );
        this.globalMin = index;
      }
      index++;
      this.generation++;
      parsed = [];
      if (index >= fitnessArray.length) {
        clearInterval(interval);
      }
    }, 2000);
    $("#stop").click(function () {
      clearInterval(interval);
    });
  }

  standardDeviation(values) {
    /*
  Calculate the standard deviation of the array passed in parameters

  Parameters
  ----------
  values : any
        the array containing the data to use
  Returns
  -------
  stdDev : number 
        the standard deviation of the values passed in parameters
  */
    var average = this.average(values);

    var squareDiffs = values.map(function (value) {
      var diff = value - average;
      var sqrDiff = diff * diff;
      return sqrDiff;
    });

    var averageSquareDiff = this.average(squareDiffs);

    var stdDev = Math.sqrt(averageSquareDiff);
    return stdDev;
  }

  average(data) {
    /*
  Calculate the average value of the data passed in parameters

  Parameters
  ----------
  data : any
        the array containing all the values of which you need the average
  Returns
  -------
  average : number
        the average of the values passed in parameters
  */
    var sum = data.reduce(function (sum, value) {
      return sum + value;
    }, 0);

    var average = sum / data.length;
    return average;
  }

  getMin(array: any[]) {
    /*
  Return the lowest value of the array

  Parameters
  ----------
  array : any[]
        the array where we need to extract the lowest value
  Returns
  -------
  min : number
      the lowest value of the array
  */
    let min: number = +parseFloat(array[0].replace(",", "."));
    let parsedValue: number;
    array.forEach(function (value) {
      parsedValue = +parseFloat(value.replace(",", "."));
      if (min > parsedValue) {
        min = parsedValue;
      }
    });
    return min;
  }

  getMax(array: any[]) {
    /*
  Return the highest value of the array

  Parameters
  ----------
  array : any[]
        the array where we need to extract the highest value
  Returns
  -------
  max : number
      the highest value of the array
  */
    let max: number = +parseFloat(array[0].replace(",", "."));
    let parsedValue: number;
    array.forEach(function (value) {
      parsedValue = +parseFloat(value.replace(",", "."));
      if (max < parsedValue) {
        max = parsedValue;
      }
    });
    return max;
  }

  setColumn(array: any[], allValueArray: any[]) {
    /*
  Set the values of the columns

  Parameters
  ----------
  array : any[]
        the array where we need to extract the values for the columns
  allValueArray : any[]
        the array where we need to extract the highest value
  Returns
  -------
  nothing
  */
    var firstTierValue = 0,
      secondTierValue = 0,
      thirdTierValue = 0,
      index = 0,
      firstTier = 0,
      secondTier = 0,
      minLocal = 0;
    var maxValue = this.getMax(allValueArray);
    let interval = setInterval(() => {
      firstTier = maxValue * 0.33;
      secondTier = maxValue * 0.66;
      array[index].forEach(function (value) {
        if (parseFloat(value) >= 0 && parseFloat(value) < firstTier) {
          firstTierValue++;
        }
        if (parseFloat(value) >= firstTier && parseFloat(value) < secondTier) {
          secondTierValue++;
        }
        if (parseFloat(value) >= secondTier) {
          thirdTierValue++;
        }
      });
      minLocal = this.getMin(array[index]);
      this.barChartData[0].data.push(minLocal);
      this.barChartData[1].data.push(firstTierValue);
      this.barChartData[2].data.push(secondTierValue);
      this.barChartData[3].data.push(thirdTierValue);
      this.barChartLabels.push(index.toString());

      firstTierValue = secondTierValue = thirdTierValue = 0;
      index++;
      if (index >= array.length) {
        clearInterval(interval);
      }
    }, 2000);
    $("#stop").click(function () {
      clearInterval(interval);
    });
  }

  fitnessSortedScatterChart(array: any[]) {
        /*
  Implement the data of the scatter chart and sort it depending of the fitness (NOT OPERATIONAL)

  Parameters
  ----------
  array : any[]
        the array where we need to extract the values for the columns
  Returns
  -------
  nothing
  */
    let index = 0;
    let valeur = 0;
    let interval = setInterval(() => {
      this.dataCyan.push({ x: +index, y: +valeur });
      index++;
      if (index < array[1].length) {
        valeur++;
        index = 0;
      }
      this.charts.forEach((child) => {
        child.chart.update();
      });
      if (index >= array.length) {
        clearInterval(interval);
      }
    }, 2000);
  }

  ageSortedScatterChart() {
        /*
  Implement the data of the scatter chart and sort it depending of the age (NOT OPERATIONAL)

  Parameters
  ----------
  none 

  Returns
  -------
  nothing
  */
    this.charts[0].chart.destroy();
    console.log(this.dataCyan);
    this.charts.forEach((child) => {
      child.chart.update();
    });
  }

  /* Options for the scatter chart*/

  public scatterChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    title: {
      display: true,
      text: "Evolution du viellissement de la population",
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: true,
          },
          scaleLabel: {
            display: true,
            labelString: "Itérations",
          },
        },
      ],
      yAxes: [
        {
          gridLines: {
            display: false,
          },
          scaleLabel: {
            display: true,
            labelString: "Individus",
          },
        },
      ],
    },
  };
  public scatterChartLabels: Label[] = [];

  public scatterChartData: ChartDataSets[] = [
    {
      data: this.dataCyan,
      backgroundColor: "#d2f1ef",
      pointBackgroundColor: "#d2f1ef",
      label: "1",
      pointRadius: 10,
      pointStyle: "rect",
    },
    {
      data: this.dataGreen,
      backgroundColor: "#24b5ac",
      pointBackgroundColor: "#24b5ac",
      label: "Entre 2 compris et 3 compris",
      pointRadius: 10,
      pointStyle: "rect",
    },
    {
      data: this.dataBlue,
      backgroundColor: "#01505c",
      pointBackgroundColor: "#01505c",
      label: "Entre 4 compris et 6 compris",
      pointRadius: 10,
      pointStyle: "rect",
    },
    {
      data: this.dataOrange,
      backgroundColor: "#ff9300",
      pointBackgroundColor: "#ff9300",
      label: "Plus de 6",
      pointRadius: 10,
      pointStyle: "rect",
    },
  ];
  public scatterChartType: ChartType = "scatter";

  public onChartClick(e: any): void {
  }

  generation = 0;

  /* Options for the gauges */
  public canvasWidth = 200;
  public needleValueFitness = 50;
  public needleValueEntropy = 50;
  public centralLabel = "";
  public nameFitness = "Fitness moyenne";
  public nameEntropy = "Entropie moyenne";
  public bottomLabelFitness = this.needleValueFitness.toString();
  public bottomLabelEntropy = this.needleValueFitness.toString();
  /* Options for the fitness gauge */
  public optionsFitness = {
    hasNeedle: true,
    needleColor: "black",
    needleUpdateSpeed: 1000,
    arcColors: ["green", "yellow", "pink", "orange", "red"],
    arcDelimiters: [20, 40, 60, 80],
    rangeLabel: ["0%", "100%"],
    arcPadding: 3,
    arcPaddingColor: "#d2f1ef",
    arcLabels: ["20%", "40%", "60%", "80%"],
  };
  /* Options for the entropy gauge */
  public optionsEntropy = {
    hasNeedle: true,
    needleColor: "black",
    needleUpdateSpeed: 1000,
    arcColors: ["red", "orange", "pink", "yellow", "green"],
    arcDelimiters: [20, 40, 60, 80],
    rangeLabel: ["0%", "100%"],
    arcPadding: 3,
    arcPaddingColor: "#d2f1ef",
    arcLabels: ["20%", "40%", "60%", "80%"],
  };

  /* Options for the bar chart */

  public barChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    title: {
      display: true,
      text: "Répartition et évolution de la fitness",
    },
    tooltips: {
      mode: "index",
      intersect: false,
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          gridLines: {
            display: true,
          },
          scaleLabel: {
            display: true,
            labelString: "Iterations",
          },
        },
      ],
      yAxes: [
        {
          stacked: true,
          gridLines: {
            display: false,
          },
          scaleLabel: {
            display: true,
            labelString: "Fitness",
          },
        },
      ],
    },
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    {
      data: [],
      backgroundColor: "#000000",
      label: "Meilleure fitness",
      type: "line",
      pointBackgroundColor: "black",
      fill: false,
      borderColor: "black",
    },
    { data: [], backgroundColor: "#d2f1ef", label: "Entre 0 et 33%" },
    { data: [], backgroundColor: "#24b5ac", label: "Entre 33 et 66%" },
    { data: [], backgroundColor: "#ff9300", label: "entre 67 et 100%" },
  ];
}
